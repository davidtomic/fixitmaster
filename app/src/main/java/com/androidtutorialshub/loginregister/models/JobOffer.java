package com.androidtutorialshub.loginregister.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 10/25/17.
 */

public class JobOffer {

    @SerializedName("job_id")
    private int jobId;
    @SerializedName("user_email")
    private String userEmail;
    @SerializedName("master_email")
    private String masterEmail;
    @SerializedName("hand_offer")
    private String handOffer;
    @SerializedName("material_offer")
    private String materialOffer;
    @SerializedName("additional_info")
    private String additionalInfo;
    @SerializedName("job_title")
    private String jobTitle;
    @SerializedName("date_offered")
    private String dateOffered;

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getMasterEmail() {
        return masterEmail;
    }

    public void setMasterEmail(String masterEmail) {
        this.masterEmail = masterEmail;
    }

    public String getHandOffer() {
        return handOffer;
    }

    public void setHandOffer(String handOffer) {
        this.handOffer = handOffer;
    }

    public String getMaterialOffer() {
        return materialOffer;
    }

    public void setMaterialOffer(String materialOffer) {
        this.materialOffer = materialOffer;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getDateOffered() {
        return dateOffered;
    }

    public void setDateOffered(String dateOffered) {
        this.dateOffered = dateOffered;
    }
}
