package com.androidtutorialshub.loginregister.httprequests;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by tomicdavid on 10/25/17.
 */

public class AddOfferRequest extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(String... params) {
        OutputStream os = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL("http://192.168.30.245/fixit/job_offers/add_job_offer.php");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("hand_offer", params[0]);
            jsonObject.put("material_offer", params[1]);
            jsonObject.put("user_email", params[2]);
            jsonObject.put("master_email", params[3]);
            jsonObject.put("additional_info", params[4]);
            jsonObject.put("job_title", params[5]);
            String message = jsonObject.toString();

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json");

            //open
            conn.connect();

            //send data
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());

            //clean up
            os.flush();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
