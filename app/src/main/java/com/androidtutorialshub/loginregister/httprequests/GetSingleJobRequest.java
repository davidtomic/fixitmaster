package com.androidtutorialshub.loginregister.httprequests;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by tomicdavid on 10/25/17.
 */

public class GetSingleJobRequest extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
        String job = "";
        OutputStream os = null;
        InputStream is = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL("http://192.168.30.245/fixit/jobs/get_job_by_email.php");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", params[0]);
            jsonObject.put("job_description", params[1]);
            String message = jsonObject.toString();

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json");

            //open
            conn.connect();

            //send data
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());

            //clean up
            os.flush();

            //do something with response
            is = conn.getInputStream();

            job = convertStreamToString(is);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
                is.close();
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        return job;
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
