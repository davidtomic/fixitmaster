package com.androidtutorialshub.loginregister.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.adapter.CommentListAdapter;
import com.androidtutorialshub.loginregister.httprequests.AddCommentRequest;
import com.androidtutorialshub.loginregister.httprequests.GetAllComments;
import com.androidtutorialshub.loginregister.httprequests.GetMaster;
import com.androidtutorialshub.loginregister.models.Comment;
import com.androidtutorialshub.loginregister.models.Master;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by tomicdavid on 10/18/17.
 */

public class MasterActivity extends AppCompatActivity {

    private Context context = this;

    String credentialEmail = "";

    private TextView name;
    private TextView email;
    private TextView business;
    private RatingBar rate;
    private View bottomToolbar;
    private ImageButton homeButton;
    private ImageButton messageButton;
    private ImageButton commentButton;
    private RecyclerView commentsView;
    private SwipeRefreshLayout refreshComments;

    private final GetAllComments commentRequests = new GetAllComments();
    private final GetMaster masterRequest = new GetMaster();
    private final AddCommentRequest addCommentRequest = new AddCommentRequest();

    private List<Comment> comments;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        credentialEmail = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        bottomToolbar = findViewById(R.id.bottom_toolbar_master);

        homeButton = (ImageButton) bottomToolbar.findViewById(R.id.home_button);
        messageButton = (ImageButton) bottomToolbar.findViewById(R.id.message_button);
        commentButton = (ImageButton) bottomToolbar.findViewById(R.id.comment_button);
        refreshComments = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        Intent intent = getIntent();

        final String intentEmail = credentialEmail;

        Master master = null;
        try {
            master = loadMaster(intentEmail);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setValuesToMaster(master);

        try {
            loadComments(intentEmail);
        } catch (Exception e) {
            e.printStackTrace();
        }

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AllJobsActivity.class);
                startActivity(intent);
            }
        });

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessagesActivity.class);
                startActivity(intent);
            }
        });

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCommentDialog(intentEmail);
            }
        });

        refreshComments.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    refreshComments(intentEmail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    void refreshComments(String intentEmail) throws Exception {

        commentsView.setAdapter(null);

        loadComments(intentEmail);

        if (refreshComments.isRefreshing()) {
            refreshComments.setRefreshing(false);
        }

    }

    public void openCommentDialog(final String intentEmail) {
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.comment_dialog, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.view.ContextThemeWrapper(MasterActivity.this, R.style.AppTheme));

                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);

                alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String result = "";
                        result = userInput.getText().toString();
                        addCommentRequest.execute(intentEmail, credentialEmail, result);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    public Master loadMaster(String intentEmail) throws Exception {

        String masterJSon = "";

        masterJSon = masterRequest.execute(intentEmail).get();

        Master master = null;

        if (isValid(masterJSon)) {
            master = new Gson().fromJson(masterJSon, Master.class);
        }

        return master;
    }

    public void setValuesToMaster(final Master master) {

        name = (TextView) findViewById(R.id.master_name_details);
        email = (TextView) findViewById(R.id.master_email_details);
        business = (TextView) findViewById(R.id.master_business_details);
        rate = (RatingBar) findViewById(R.id.master_rate_details);
        rate.setRating(2.5f);

        name.setText(master.getName());
        email.setText(master.getEmail());
        business.setText(master.getBussiness());

    }

    public static boolean isValid(final String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

    public void loadComments(final String intentEmail) throws Exception {
        commentsView = (RecyclerView) findViewById(R.id.master_comments);
        commentsView.setLayoutManager(new LinearLayoutManager(this));

        String commentResponse = "";

        commentResponse = commentRequests.execute(intentEmail).get();

        Type listType = new TypeToken<List<Comment>>() {
        }.getType();

        comments = new Gson().fromJson(commentResponse, listType);

        commentsView.setAdapter(new CommentListAdapter(comments, context));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.all_jobs:
                startActivity(new Intent(this, AllJobsActivity.class));
                return true;
            case R.id.logout:
                SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("credentials", "");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
