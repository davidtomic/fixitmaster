package com.androidtutorialshub.loginregister.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.httprequests.LoginRequests;
import com.androidtutorialshub.loginregister.httprequests.RegisterRequest;

/**
 * Created by tomicdavid on 10/17/17.
 */

public class RegisterActivity extends AppCompatActivity {

    private Context context;

    private EditText username;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private Button registerButton;
    private TextView registerToLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_master);

        username = (EditText)findViewById(R.id.register_name);
        email = (EditText)findViewById(R.id.register_email);
        password = (EditText)findViewById(R.id.register_password);
        confirmPassword = (EditText)findViewById(R.id.register_confirm_password);
        registerButton = (Button)findViewById(R.id.register_button);
        registerToLogin = (TextView)findViewById(R.id.register_to_login_button);
        context = getApplicationContext();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getText().toString().length() > 5 && email.getText().toString().contains("@")) {
                    if (password.getText().toString().equals(confirmPassword.getText().toString())) {
                        RegisterRequest registerRequest = new RegisterRequest();
                        try {
                            registerRequest.execute(username.getText().toString(), email.getText().toString(), password.getText().toString());
                            Toast.makeText(getApplicationContext(), "User Registered successfully", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "HttpRequestFailed", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Email and password for security please.", Toast.LENGTH_LONG).show();
                }
            }
        });

        registerToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }
}
