package com.androidtutorialshub.loginregister.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.httprequests.GetSingleJobRequest;
import com.androidtutorialshub.loginregister.models.Job;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;

/**
 * Created by tomicdavid on 10/24/17.
 */

public class SingleJobActivity extends Activity {

    private TextView job_owner;
    private TextView job_location;
    private TextView job_number;
    private TextView job_title;
    private TextView job_description;

    private Job job;

    private String job_JSon;
    private String credentialEmail;

    private Context context;

    private View view;

    private ImageButton homeButton;
    private ImageButton messageButton;
    private Button applyJob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_job);

        context = getApplicationContext();

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        credentialEmail = prefs.getString("credentials", "default_value_here_if_string_is_missing");

        if(credentialEmail.isEmpty()){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        Intent intent = getIntent();
        final String jobOwner = intent.getStringExtra("email");
        final String jobDescription = intent.getStringExtra("desc");

        job_owner = (TextView)findViewById(R.id.item_job_username);
        job_location = (TextView)findViewById(R.id.item_job_location);
        job_number = (TextView)findViewById(R.id.item_job_number);
        job_title = (TextView)findViewById(R.id.item_job_name);
        job_description = (TextView)findViewById(R.id.item_job_description);

        view = findViewById(R.id.bottom_toolbar_jobs);

        homeButton = view.findViewById(R.id.job_home_button);
        messageButton = view.findViewById(R.id.message_job_owner);
        applyJob = view.findViewById(R.id.apply_now);

        GetSingleJobRequest getSingleJobRequest = new GetSingleJobRequest();

        try {
            job_JSon = getSingleJobRequest.execute(jobOwner, jobDescription).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Type jobType = new TypeToken<Job>(){}.getType();

        if (isValid(job_JSon)) {
            job = new Gson().fromJson(job_JSon, jobType);
        }

        job_owner.setText(job.getJobOwner());
        job_location.setText(job.getJobLocation());
        job_number.setText(job.getJobPhoneNumber());
        job_title.setText(job.getJobTitle());
        job_description.setText(job.getJobDescription());

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AllJobsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessagesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        applyJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ApplyForJobActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("email", jobOwner);
                intent.putExtra("desc", jobDescription);
                startActivity(intent);
                finish();
            }
        });

    }

    public static boolean isValid(String json){
        try{
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

}
