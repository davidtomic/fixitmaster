package com.androidtutorialshub.loginregister.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.httprequests.AddOfferRequest;

/**
 * Created by tomicdavid on 10/25/17.
 */

public class ApplyForJobActivity extends Activity {

    private Context context;
    private EditText hand_offer;
    private EditText material_offer;
    private EditText additional_info;
    private TextView job_title;

    private final AddOfferRequest addOfferRequest = new AddOfferRequest();
    private Button apply;
    private View view;
    private ImageButton homeButton;
    private ImageButton messageButton;

    private Intent intent;

    private String credentialEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_for_job);

        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        final String master = prefs.getString("credentials", "default_value_here_if_string_is_missing");
        credentialEmail = master;

        context = this;
        if(master.isEmpty()){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        intent = getIntent();
        final String jobOwner = intent.getStringExtra("email");
        String jobDesc = intent.getStringExtra("desc");

        view = findViewById(R.id.bottom_toolbar_apply_job);

        hand_offer = findViewById(R.id.hands_offer);
        material_offer = findViewById(R.id.material_offer);
        additional_info = findViewById(R.id.additional_info);
        job_title = findViewById(R.id.apply_job_title);
        apply = view.findViewById(R.id.apply);
        homeButton = view.findViewById(R.id.home_button);
        messageButton = view.findViewById(R.id.message_button);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOfferRequest.execute(hand_offer.getText().toString(), material_offer.getText().toString(), jobOwner ,master, additional_info.getText().toString(), job_title.getHint().toString());
                Toast.makeText(context, "Offer sent.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, AllJobsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AllJobsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessagesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(credentialEmail.length()==0){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        menu.getItem(R.id.all_jobs).setVisible(false);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.logout:
                SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("credentials", "");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
