package com.androidtutorialshub.loginregister.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.activities.MasterActivity;
import com.androidtutorialshub.loginregister.models.Master;

import java.util.List;

/**
 * Created by tomicdavid on 10/17/17.
 */

public class MasterListAdapter extends RecyclerView.Adapter<MasterListAdapter.ViewHolder> {

    private List<Master> list;
    private Context context;

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView masterName;
        TextView masterEmail;
        TextView masterPassword;
        TextView masterBusiness;
        TextView masterHaveLicence;

        public ViewHolder(View itemView) {
            super(itemView);

            masterName = (TextView)itemView.findViewById(R.id.master_name);
            masterEmail = (TextView)itemView.findViewById(R.id.master_email);
            masterPassword = (TextView)itemView.findViewById(R.id.master_password);
            masterBusiness = (TextView)itemView.findViewById(R.id.master_business);
            masterHaveLicence = (TextView)itemView.findViewById(R.id.master_have_licence);

            masterName.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            masterEmail.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            masterPassword.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            masterBusiness.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            masterHaveLicence.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MasterActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("email", masterEmail.getText().toString());
                    context.startActivity(intent);
                }
            });
        }
    }

    public MasterListAdapter(List<Master> list, Context context){
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_master, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MasterListAdapter.ViewHolder holder, int position) {
        final Master master = list.get(position);

        String name = master.getName();
        String email = master.getEmail();
        String password = master.getPassword();
        String business = master.getBussiness();
        String haveLicence = master.haveLicence();

        if(haveLicence.equals("t")){
            haveLicence = "true";
        } else {
            haveLicence = "false";
        }

        holder.masterName.setText(name);
        holder.masterEmail.setText(email);
        holder.masterPassword.setText(password);
        holder.masterBusiness.setText(business);
        holder.masterHaveLicence.setText(haveLicence);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
