package com.androidtutorialshub.loginregister.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.activities.MasterActivity;
import com.androidtutorialshub.loginregister.models.Comment;

import java.util.List;

/**
 * Created by tomicdavid on 10/19/17.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {

    private List<Comment> list;
    private Context context;

    public CommentListAdapter(List<Comment> comments, Context context){
        this.list = comments;
        this.context = context;
    }

    @Override
    public CommentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new CommentListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        final Comment comment = list.get(position);

        String owner = comment.getCommentOwner();
        String body = comment.getBody();

        holder.comment_owner.setText(owner);
        holder.comment_body.setText(body);

    }

    @Override
    public int getItemCount() {
        if(list == null){return 0;}
        if(!list.isEmpty()&&list!=null) {
            return list.size();
        } else{
            return 0;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView comment_body;
        TextView comment_owner;

        public ViewHolder(View itemView) {
            super(itemView);

            comment_body = (TextView)itemView.findViewById(R.id.comment_body);
            comment_owner = (TextView)itemView.findViewById(R.id.commentOwner);

            comment_body.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            comment_owner.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MasterActivity.class);
                    intent.putExtra("email", comment_owner.getText().toString());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

        }
    }
}
