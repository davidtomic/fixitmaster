package com.androidtutorialshub.loginregister.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.activities.SingleJobActivity;
import com.androidtutorialshub.loginregister.models.Job;

import java.util.List;

/**
 * Created by tomicdavid on 10/24/17.
 */

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.ViewHolder>  {

    private List<Job> jobList;
    private Context context;

    public JobListAdapter(List<Job> jobs, Context context){
        this.jobList = jobs;
        this.context = context;
    }

    @Override
    public JobListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job, parent, false);
        return new JobListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(JobListAdapter.ViewHolder holder, int position) {
        final Job job = jobList.get(position);

        String job_owner = job.getJobOwner();
        String job_location = job.getJobLocation();
        String job_phone_number = job.getJobPhoneNumber();
        String job_title = job.getJobTitle();
        String job_description = job.getJobDescription();

        holder.job_owner.setText(job_owner);
        holder.job_location.setText(job_location);
        holder.job_phone_number.setText(job_phone_number);
        holder.job_title.setText(job_title);
        holder.job_description.setText(job_description);
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView job_owner;
        TextView job_location;
        TextView job_phone_number;
        TextView job_title;
        TextView job_description;

        public ViewHolder(View itemView) {
            super(itemView);

            job_owner = (TextView)itemView.findViewById(R.id.item_job_username);
            job_location = (TextView)itemView.findViewById(R.id.item_job_location);
            job_phone_number = (TextView)itemView.findViewById(R.id.item_job_number);
            job_title = (TextView)itemView.findViewById(R.id.item_job_name);
            job_description = (TextView)itemView.findViewById(R.id.item_job_description);


            job_owner.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            job_location.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            job_phone_number.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            job_title.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            job_description.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SingleJobActivity.class);
                    intent.putExtra("email", job_owner.getText().toString());
                    intent.putExtra("desc", job_description.getText().toString());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

        }
    }
}
